<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MovimientosControllers extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			$this->load->model('movimientos/MovimientosModel', 'ModeloMovimientos');
	}

	public function index()
	{
		redirect('empleados');
	}

	public function ObtenerMovimientos($numeroEmpleado){

		$respuesta = false;
		try {
			
            $respuesta['movimientos'] = $this->ModeloMovimientos->obtenerMovimientos($numeroEmpleado);
            $empleado = $this->ModeloMovimientos->buscarEmpleado($numeroEmpleado);
			if(!$empleado){
				
				throw new Exception('No existen empleados',200);
			}else{
                $respuesta['empleado'] = $empleado;
            }
		}  catch (Exception $ex) {
            $mensaje = $ex->getMessage();
            log_message('error', $mensaje);
            $mensajeAlerta['correcto']=false;
            $mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
            $this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
            redirect('empleados');
			
		}

		$this->load->view('includes/header');
		$this->load->view('movimientos/MovimientosView', $respuesta);
	}

	public function buscarEmpleado($numEmpleado){

		$respuesta = false;
		try {
			
			$respuesta = $this->ModeloMovimientos->buscarEmpleado($numEmpleado);

			if(!$respuesta){
				
				throw new Exception('el empleado no existe',200);
			}
		}  catch (Exception $ex) {

			$mensaje = $ex->getMessage();
			log_message('error', $mensaje);
			
		}
	}

	public function guardarMovimientos($numeroEmpleado){

		$mensajeAlerta = null;
		$respuesta = null;
        $movimiento = null;
        $empleado = null;
        
        try {
				
            $empleado = $this->ModeloMovimientos->buscarEmpleado($numeroEmpleado);

            if(!$empleado){

                throw new Exception('El empleado no existe.',200);

            }else{
                $datos['empleado'] = $empleado;
            }
        }  catch (Exception $ex) {
            $mensaje = $ex->getMessage();
            log_message('error', $mensaje);
            $mensajeAlerta['correcto']=false;
            $mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
            $this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
            redirect('empleados');
        }

		$this->form_validation->set_error_delimiters('<p class="text-warning">', '</p>');
        $this->form_validation->set_rules('entregas', 'entregas', 'required');
        $this->form_validation->set_rules('fecha', 'fecha', 'required');

        $cubrio = ($this->input->post('cubrio')) ? true : false ;

        if($cubrio){

        $this->form_validation->set_rules('role', 'Rol que cubre', 'required|callback_validaSeleccion');

       }
        
		if ($this->form_validation->run() == FALSE){
		
			$this->load->view('includes/header');
			$this->load->view('movimientos/MovimientosFormView', $datos);
		}
		else{
			
			$respuesta = false;
			$movimiento = $this->input->post();
			
			try {

				$respuesta = $this->ModeloMovimientos->guardarMovimiento($movimiento);

				if(!$respuesta){
					throw new Exception('Fallo al intentar guardar los datos',500);

				}else{
					$mensajeAlerta['correcto']=true;
					$mensajeAlerta['mensaje']='Movimiento correctamente registrado.';
					$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
				}
			}  catch (Exception $ex) {	
				$mensaje = $ex->getMessage();
                log_message('error', $mensaje);
                $mensajeAlerta['correcto']=false;
                $mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
                $this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
                redirect('movimientos'.$numeroEmpleado);

			}
			redirect('movimientos/'.$numeroEmpleado);
		}
	}


	public function eliminarMovimientos($numeroMovimiento, $numeroEmpleado){
		
		$respuesta = false;
		try {
			
			$respuesta = $this->ModeloMovimientos->eliminarMovimientos($numeroMovimiento);

			if(!$respuesta){

                throw new Exception('Error al tratar de eliminar a el Movimiento.',200);
                
			}else{
				$mensajeAlerta['correcto']=true;
				$mensajeAlerta['mensaje']= 'Movimiento eliminado';
				$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
				redirect('movimientos/'.$numeroEmpleado);
			}
		}  catch (Exception $ex) {
			$mensaje = $ex->getMessage();
			log_message('error', $mensaje);
			$mensajeAlerta['correcto']=false;
			$mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
			$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
			redirect('movimientos/'.$numeroEmpleado);
		}
	}

	//validacion a control select
	public function validaSeleccion($tipoEmpleado){
		if ($tipoEmpleado == 'Seleccionar')
		{
				$this->form_validation->set_message('validaSeleccion', 'Favor de seleccionar una opción diferente a "Seleccionar"');
				return FALSE;
		}
		else
		{
				return TRUE;
		}
	}

	//funcion usada para crear los mensajes de alerta
	function formatoMensajeAlerta($mensajeAlerta){
		if($mensajeAlerta['correcto']){
			$mensaje='<div class="alert alert-success" alert-dismissible fade show" role="alert" ><center><i class="fas fa-check-circle"></i><strong>'.$mensajeAlerta['mensaje'].'</strong></center></div>';
		}else{
			$mensaje='<div class="alert alert-danger" alert-dismissible fade show" role="alert" ><center><i class="fas fa-exclamation-triangle"></i><strong>'.$mensajeAlerta['mensaje'].'</strong></center></div>';
		} 
		return $mensaje;
	  }
}
