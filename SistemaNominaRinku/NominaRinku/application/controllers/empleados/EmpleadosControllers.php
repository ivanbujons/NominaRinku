<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmpleadosControllers extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			// Your own constructor code
			$this->load->model('empleados/EmpleadosModel', 'ModeloEmpleados');
			// $this->load->library("session");
			$this->load->helper(array('form', 'url'));
			// $this->load->library('form_validation');
			$this->load->library('Pdf');

	}

	public function index()
	{
		redirect('empleados');
	}

	//Obtiene un listado de los empleados y sus datos
	public function ObtenerEmpleados(){

		$respuesta = false;
		try {
			
			$respuesta['empleados'] = $this->ModeloEmpleados->obtenerEmpleados();

			if(!$respuesta){
				
				throw new Exception('No existen empleados',200);
			}
		}  catch (Exception $ex) {

			$mensaje = $ex->getMessage();
			log_message('error', $mensaje);
			
		}


		$this->load->view('includes/header');
		$this->load->view('empleados/EmpleadosView', $respuesta);


	}

	public function buscarEmpleado($numEmpleado){

		$respuesta = false;
		try {
			
			$respuesta = $this->ModeloEmpleados->buscarEmpleado($numEmpleado);

			if(!$respuesta){
				
				throw new Exception('el empleado no existe',200);
			}
		}  catch (Exception $ex) {

			$mensaje = $ex->getMessage();
			log_message('error', $mensaje);
			
		}

	}

	public function guardarEmpleado(){

		$mensajeAlerta = null;
		$respuesta = null;
		$empleado = null;

		//validaciones a campos
		$this->form_validation->set_error_delimiters('<p class="text-warning">', '</p>');
		$this->form_validation->set_rules('nombre', 'nombre', 'min_length[4]|max_length[50]|alpha_numeric_spaces|required');
		$this->form_validation->set_rules('appelidoPaterno', 'Appelido Paterno', 'min_length[4]|max_length[50]|alpha_numeric_spaces|required');
		$this->form_validation->set_rules('appelidoMaterno', 'AppelidoMaterno', 'min_length[4]|max_length[50]|alpha_numeric_spaces|required');
		$this->form_validation->set_rules('role', 'role', 'required|callback_validaSeleccion');	
		$this->form_validation->set_rules('tipoEmpleado', 'Tipo Empleado', 'required|callback_validaSeleccion');
		
		//Fallo validaciones, envia mensajes a vista
		if ($this->form_validation->run() == FALSE){
		
			$this->load->view('includes/header');
			$this->load->view('Empleados/EmpleadosFormView');
		}
		else{
			//Paso validaciones, procede a enviar datos a modelo
			$respuesta = false;
			$empleado = $this->input->post();

			try {

				$respuesta = $this->ModeloEmpleados->guardarEmpleado($empleado);

				if(!$respuesta){
					throw new Exception('Error al guardar',500);
					$mensajeAlerta['correcto']=false;
            		$mensajeAlerta['mensaje']='Fallo al intentar guardar los datos. Póngase en contacto con el soporte técnico.';
					$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));

				}else{
					$mensajeAlerta['correcto']=true;
					$mensajeAlerta['mensaje']='Empleado correctamente registrado.';
					$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
				}
			}  catch (Exception $ex) {	
				$mensaje = $ex->getMessage();
				log_message('error', $mensaje);

			}
			redirect('empleados');
		}
	}

	public function eliminarEmpleado($numeroEmpleado){
		
		$respuesta = false;
		try {
			
			$respuesta = $this->ModeloEmpleados->buscarEmpleado($numeroEmpleado);
			//si no encontro el empleado se envia excepcion
			if(!$respuesta){
				throw new Exception('El empleado no existe.',200);
			}else{

				$respuesta = $this->ModeloEmpleados->eliminarEmpleado($numeroEmpleado);

				if(!$respuesta){

					throw new Exception('Error al tratar de eliminar a el empleado.',200);
				}else{
					//se elimino correctamente, regresa al listado
					$mensajeAlerta['correcto']=true;
					$mensajeAlerta['mensaje']= 'Empleado eliminado';
					$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
					redirect('empleados');
				}
			}
		}  catch (Exception $ex) {
			//se envia mensaje de que fallo, regresa al listado
			$mensaje = $ex->getMessage();
			log_message('error', $mensaje);
			$mensajeAlerta['correcto']=false;
			$mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
			$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
			redirect('empleados');
		}
	}

	public function modificarEmpleados($numeroEmpleado){

		$mensajeAlerta = null;
		$respuesta = null;
		$empleado = null;
		$empleado['bandera']=false;
		$empleado['numeroEmpleado']=$numeroEmpleado;

		if(!$_POST){
			try {
				
				$respuesta = $this->ModeloEmpleados->buscarEmpleado($numeroEmpleado);

				if(!$respuesta){

					throw new Exception('El empleado no existe.',200);

				}else{

					$empleado['empleado'] = $respuesta;
					$empleado['bandera']=true;
				}
			}  catch (Exception $ex) {
				$mensaje = $ex->getMessage();
				log_message('error', $mensaje);
				$mensajeAlerta['correcto']=false;
				$mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
				$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
				redirect('empleados');
			}
		}
		//validaciones a campos
		$this->form_validation->set_error_delimiters('<p class="text-warning">', '</p>');
		$this->form_validation->set_rules('nombre', 'nombre', 'min_length[4]|max_length[50]|alpha_numeric_spaces|required');
		$this->form_validation->set_rules('appelidoPaterno', 'Appelido Paterno', 'min_length[4]|max_length[50]|alpha_numeric_spaces|required');
		$this->form_validation->set_rules('appelidoMaterno', 'AppelidoMaterno', 'min_length[4]|max_length[50]|alpha_numeric_spaces|required');
		$this->form_validation->set_rules('role', 'role', 'required|callback_validaSeleccion');	
		$this->form_validation->set_rules('tipoEmpleado', 'Tipo Empleado', 'required|callback_validaSeleccion');
		
		//Fallo validaciones, envia mensajes a vista
		if ($this->form_validation->run() == FALSE){
		
			$this->load->view('includes/header');
			$this->load->view('Empleados/EmpleadosModificacionFromView', $empleado);
		}
		else{
			//Paso validaciones, procede a enviar datos a modelo
			$respuesta = false;
			$empleado = $this->input->post();
			
			try {

				$respuesta = $this->ModeloEmpleados->modificarEmpleado($numeroEmpleado, $empleado);

				if(!$respuesta){
					throw new Exception(' No se modificaron los datos ',500);

				}else{
					$mensajeAlerta['correcto']=true;
					$mensajeAlerta['mensaje']=' Empleado actualizado correctamente.';
					$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
				}
			}  catch (Exception $ex) {	
				$mensaje = $ex->getMessage();
				log_message('error', $mensaje);
				$mensajeAlerta['correcto']=false;
				$mensajeAlerta['mensaje']= $mensaje.'.';
				$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
				redirect('empleados');

			}
			redirect('empleados');
		}
	}	

	public function generarNomina($numeroEmpleado){
	
		$sueldoHora = 30;
		$jornadaDiaria = 8;
		$diasLaboralesMensual = 30;
		$bonoPorEntrega = 5;
		$bonoPorHoraRolTipo1 = 10;
		$bonoPorHoraRolTipo2 = 5;
		$porcentajeISR = 0.09;
		$porcentajeISREspecial = 0.12;
		$topeSueldoISREspecial = 16000;
		$ISR = null;
		$sueldoBaseMensual = null;
		$sueldoBonoHoraMensual = null;
		$horasLabolaresMensual = null;
		$empleado=null;
		$movimientos= null;
		$valeDespensa = null;
		$tieneDerechoDespensa = false;
		$porcentajeDespensa = 0.04;
		$anio = date('Y');
		$mes = date('m');
		$pagoEntregas = null;
		$diasCubrio = null;
		$sueldo=null;
		$sueldoInicial=null;

		try {
				
			$empleado = $this->ModeloEmpleados->buscarEmpleado($numeroEmpleado);

			if(!$empleado){

				throw new Exception('El empleado no existe.',200);

			}

			$entregas = $this->ModeloEmpleados->obtenerEntregas($numeroEmpleado, $anio, $mes);

		}  catch (Exception $ex) {
			$mensaje = $ex->getMessage();
			log_message('error', $mensaje);
			$mensajeAlerta['correcto']=false;
			$mensajeAlerta['mensaje']= $mensaje.'. Póngase en contacto con el soporte técnico.';
			$this->session->set_flashdata('mensaje', $this->formatoMensajeAlerta($mensajeAlerta));
			redirect('empleados');
		}

		$horasLabolaresMensual = $jornadaDiaria * $diasLaboralesMensual;
		
		foreach ($empleado as $value) {
			if($value->idRole == 1){
				//es chofer
				$sueldoBonoHoraMensual =  $horasLabolaresMensual * $bonoPorHoraRolTipo1;
	
			}elseif($value->idRole == 2){
				//es cargador
				$sueldoBonoHoraMensual =  $horasLabolaresMensual * $bonoPorHoraRolTipo2;

			}else{
				//es auxiliar
				$sueldoBonoHoraMensual = 0;
				//consulta si cubrio dias
				$diasCubrio = $this->ModeloEmpleados->obtenerDias($numeroEmpleado, $anio, $mes);

				if($diasCubrio){

					foreach ($diasCubrio as $dias) {
						//cubrio chofer
						if($dias->idRol == 1){

							$sueldoBonoHoraMensual += $dias->numeroDias * $jornadaDiaria * $bonoPorHoraRolTipo1;

						}else{
							//cubrio cargador
							$sueldoBonoHoraMensual += $dias->numeroDias * $jornadaDiaria * $bonoPorHoraRolTipo2;
							
						}
					}
				}
			}

			if($value->idTipo == 1){
				//es interno
				$tieneDerechoDespensa = true;
			}

		}
		//se obtiene el pago por entregas
		if($entregas){

			foreach($entregas as $entrega){

				$pagoEntregas =  $bonoPorEntrega * $entrega->numeroEntregas;

			}
		}
		//sueldo base mensual - horas trabajadas del mes  * sueldo sueldo por hora
		$sueldoBaseMensual = $horasLabolaresMensual * $sueldoHora;
		$sueldoInicial = $sueldoBaseMensual;
		//Se egrega el sueldo bono hora -choferes , cargadores y auxiliares
		$sueldoBaseMensual = $sueldoBonoHoraMensual + $sueldoBaseMensual;
		//se agrega el pago por las entregas
		$sueldoBaseMensual = $pagoEntregas + $sueldoBaseMensual;
		//se calcula la despensa
		if($tieneDerechoDespensa){

			$valeDespensa = $sueldoBaseMensual * $porcentajeDespensa;

		}else{

			$valeDespensa = 0;
		}	
		//se calcula el ISR
		$ISR = ($sueldoBaseMensual > $topeSueldoISREspecial) ? $porcentajeISREspecial * $sueldoBaseMensual : $porcentajeISR * $sueldoBaseMensual ;
		//se decueta ISR
		$sueldo = $sueldoBaseMensual - $ISR;
		$sueldo = $sueldo + $valeDespensa;

		foreach ($empleado as $row) {
			
		}
		//inicia creación de reporte
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Nomina Rinku');
		$pdf->SetHeaderMargin(30);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Ivan Bujons');
		$pdf->SetDisplayMode('real', 'default');

		$pdf->AddPage();

		$html = <<<EOT
		<h1>Pago Nómina Rinku</h1>
		<div>
			<p class="first">Nombre : $row->nombreEmpleado  $row->apellidoPaterno $row->apellidoMaterno</p>
			<p class="first">Rol    : $row->roleDescripcion</p>
			<p class="first">Tipo   : $row->tipoDescripcion </p>
		<div>
		<table>
		<tr>
		  <th>Movimiento</th>
		  <th>Ingresos</th>
		  <th>Egresos</th>
		</tr>
		<tr>
		  <td>Sueldo Mensual</td>
		  <td>$$sueldoInicial</td>
		</tr>
		<tr>
		  <td>Pago Entregas</td>
		  <td>$$pagoEntregas</td>
		</tr>
		<tr>
		  <td>Pago Especial Horas</td>
		  <td>$$sueldoBonoHoraMensual</td>
		</tr>
		<tr>
		<td></td>	
		</tr>
		<tr>
		  <td>Sueldo Mensual</td>
		  <td>$$sueldoBaseMensual</td>
		</tr>
		<tr>
		  <td>ISR</td>
		  <td></td>
		  <td>$$ISR</td>
		</tr>
		<tr>
		  <td>Despensa</td>
		  <td>$$valeDespensa</td>
		  <td></td>
		</tr>
		<tr>
		  <td>Sueldo Total Mensual</td>
		  <td>$$sueldo</td>
		<td></td>
	  </tr>
	  </table>
		
EOT;
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->Output($row->nombreEmpleado.'RinkuNomina.pdf', 'D');
	}

	//validacion a control select
	public function validaSeleccion($tipoEmpleado){
		if ($tipoEmpleado == 'Seleccionar')
		{
				$this->form_validation->set_message('validaSeleccion', 'Favor de seleccionar una opción diferente a "Seleccionar"');
				return FALSE;
		}
		else
		{
				return TRUE;
		}
	}

	//funcion usada para crear los mensajes de alerta
	function formatoMensajeAlerta($mensajeAlerta){
		if($mensajeAlerta['correcto']){
			$mensaje='<div class="alert alert-success" alert-dismissible fade show" role="alert" ><center><i class="fas fa-check-circle"></i><strong>'.$mensajeAlerta['mensaje'].'</strong></center></div>';
		}else{
			$mensaje='<div class="alert alert-danger" alert-dismissible fade show" role="alert" ><center><i class="fas fa-exclamation-triangle"></i><strong>'.$mensajeAlerta['mensaje'].'</strong></center></div>';
		} 
		return $mensaje;
	  }
}
