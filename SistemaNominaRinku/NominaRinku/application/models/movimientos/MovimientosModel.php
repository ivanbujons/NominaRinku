<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 Class MovimientosModel extends CI_Model{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function obtenerMovimientos($numeroEmpleado){

        $response = null;
        
        $sql = "CALL proc_consultamovimientos(?)";
        $data = $this->db->query($sql, $numeroEmpleado );
        $result = $data->result();

        foreach ($result as $row)
        {
            $respuesta = new stdClass;
            $respuesta->numeroEmpleado         =  $row->numero_empleado;
            $respuesta->nombreEmpleado         =  $row->nombre;
            $respuesta->apellidoPaterno        =  $row->apellido_paterno;
            $respuesta->apellidoMaterno        =  $row->apellido_materno;
            $respuesta->idRole                 =  $row->id_role;
            $respuesta->roleDescripcion        =  $row->role_descripcion;
            $respuesta->idTipo                 =  $row->id_tipo;
            $respuesta->tipoDescripcion        =  $row->tipo_descripcion;
            $respuesta->numeroMovimiento       =  $row->id_movimiento;
            $respuesta->fecha                  =  $row->fecha;
            $respuesta->numeroEntregas         =  $row->entregas;
            $respuesta->rolCubierto            =  $row->rol_cubierto;
            $respuesta->descripcionRolCubierto =  $row->descripcion_rol_cubierto;


            $response[] = $respuesta;
        }
        $data->next_result();
        $data->free_result();
        return $response;
    }

    public function buscarEmpleado($numeroEmpleado){

        $response = null;
        $sql = "CALL proc_obtenerempleado(?)";
        $data = $this->db->query($sql, $numeroEmpleado );

        $result = $data->result();

        if ($data->num_rows() > 0 ) {
            foreach ($result as $row)
            {
                $respuesta = new stdClass;
                $respuesta->numeroEmpleado  =  $row->numero_empleado;
                $respuesta->nombreEmpleado  =  $row->nombre.' '.$row->apellido_paterno.' '.$row->apellido_materno;
                $respuesta->idRole          =  $row->id_role;
                $respuesta->roleDescripcion =  $row->role_descripcion;
                $respuesta->idTipo          =  $row->id_tipo;
                $respuesta->tipoDescripcion =  $row->tipo_descripcion;
    
                $response[] = $respuesta;
            }
         }else{
             
            $response = false;
          }
       
          $data->next_result();
          $data->free_result();
        return $response;
    }    

    public function guardarMovimiento($movimiento){
        $idRole = null;
        $response = null;

        unset($movimiento['nombre']);
        unset($movimiento['rol']);
        unset($movimiento['tipo']);
        unset($movimiento['cubrio']);

        $idRole = $movimiento['idRol'];   
        unset($movimiento['idRol']);
        if($movimiento['role'] == "Seleccionar"){
            $movimiento['role'] = $idRole;
        }   
        $response = false;
    

        $sql = "CALL proc_guardarmovimiento(?,?,?,?)";
        $data = $this->db->query($sql, $movimiento);

        if($this->db->affected_rows() >=1){

            $response = true;
            
        }else{

            $response = false;
        }
        return $response;
    } 

    public function eliminarMovimientos($numeroMovimiento){

        $response = false;

        $sql = "CALL proc_eliminarmovimiento(?)";
        $data = $this->db->query($sql, $numeroMovimiento);

        if($this->db->affected_rows() >=1){

            $response = true;;
            
        }else{

            $response = false;;
        }
        return $response;
    } 

}