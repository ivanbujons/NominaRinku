<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 Class EmpleadosModel extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    public function obtenerEmpleados(){

        $response = null;
        $data = $this->db->query("CALL proc_consultaempleados()");
        $result = $data->result();

        foreach ($result as $row)
        {
            $respuesta = new stdClass;
            $respuesta->numeroEmpleado  =  $row->numero_empleado;
            $respuesta->nombreEmpleado  =  $row->nombre;
            $respuesta->apellidoPaterno =  $row->apellido_paterno;
            $respuesta->apellidoMaterno =  $row->apellido_materno;
            $respuesta->idRole          =  $row->id_role;
            $respuesta->roleDescripcion =  $row->role_descripcion;
            $respuesta->idTipo          =  $row->id_tipo;
            $respuesta->tipoDescripcion =  $row->tipo_descripcion;

            $response[] = $respuesta;
        }

        return $response;
    }

    public function buscarEmpleado($numeroEmpleado){

        $response = null;
        $sql = "CALL proc_obtenerempleado(?)";
        $data = $this->db->query($sql, $numeroEmpleado );

        $result = $data->result();

        if ($data->num_rows() > 0 ) {
            foreach ($result as $row)
            {
                $respuesta = new stdClass;
                $respuesta->numeroEmpleado  =  $row->numero_empleado;
                $respuesta->nombreEmpleado  =  $row->nombre;
                $respuesta->apellidoPaterno =  $row->apellido_paterno;
                $respuesta->apellidoMaterno =  $row->apellido_materno;
                $respuesta->idRole          =  $row->id_role;
                $respuesta->roleDescripcion =  $row->role_descripcion;
                $respuesta->idTipo          =  $row->id_tipo;
                $respuesta->tipoDescripcion =  $row->tipo_descripcion;
    
                $response[] = $respuesta;
            }
         }else{
             
            $response = false;
          }
       
          $data->next_result();
          $data->free_result();
        return $response;
    }    

    public function guardarEmpleado($empleado){

        $response = false;

        $sql = "CALL proc_guardarempleado(?,?,?,?,?)";
        $data = $this->db->query($sql, $empleado);

        if($this->db->affected_rows() >=1){

            $response = true;
            
        }else{

            $response = false;
        }
        return $response;
    } 

    public function eliminarEmpleado($numeroEmpleado){

        $response = false;

        $sql = "CALL proc_eliminarempleado(?)";
        $data = $this->db->query($sql, $numeroEmpleado);

        if($this->db->affected_rows() >=1){

            $response = true;;
            
        }else{

            $response = false;;
        }
        return $response;
    } 

    public function modificarEmpleado($numeroEmpleado, $empleado){
        
        $response = false;
        $empleado = array('numerpEmpleado'=>$numeroEmpleado)+$empleado;
        
        $sql = "CALL proc_modificarempleado(?,?,?,?,?,?)";
        $data = $this->db->query($sql, $empleado);

        if($this->db->affected_rows() >=1){

            $response = true;
            
        }else{

            $response = false;
        }
        
        return $response;
    } 

    public function obtenerEntregas($numeroEmpleado, $anio, $mes){

        $response = null;
        $datos = array($numeroEmpleado, $anio, $mes);
        $response = null;

        $sql = "CALL proc_obtenerentregas(?,?,?)";
        $data = $this->db->query($sql, $datos );
        $result = $data->result();

        foreach ($result as $row)
        {
            $respuesta = new stdClass;
            $respuesta->numeroEntregas =  $row->entregas;
            $response[] = $respuesta;
        }
        $data->next_result();
        $data->free_result();
        return $response;
    }

    public function obtenerDias($numeroEmpleado, $anio, $mes){

        $response = null;
        $datos = array($numeroEmpleado, $anio, $mes);

        $sql = "CALL proc_obtenerdias(?,?,?)";
        $data = $this->db->query($sql, $datos );
        $result = $data->result();

        foreach ($result as $row)
        {
            $respuesta = new stdClass;
            $respuesta->numeroDias = $row->dias;
            $respuesta->idRol      = $row->rol_cubierto;
            $response[] = $respuesta;
        }
        $data->next_result();
        $data->free_result();
        return $response;
    }
}