<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Empleados</title>
</head>
	<body>


	<div class="container">
  		<div class="row justify-content-center">
    		<div class="col-md-18">

				<div class="card text-center">
				<div class="card-header">
					<h5>Lista de Empleados</h5>
				</div>
				<div class="card-body">

				<div class="row justify-content-left">		
					<div class="card-actions">
						<a href="nuevo/empleado" tooltip="Agregar Nuevo" class="btn btn-info" href="#">
						<i class="fas fa-user-plus"></i>
						Nuevo
						</a>
					</div>	
				</div>
				
				<table class="table">
					<thead>
						<tr>
						<th scope="col">Número Empleado</th>
						<th scope="col">Nombre</th>
						<th scope="col">Apellido Paterno</th>
						<th scope="col">Apellido Materno</th>
						<th scope="col">Rol</th>
						<th scope="col">Tipo Empleado</th>
						<th scope="col"></th>
						<th scope="col"></th>
						<th scope="col"></th>
						</tr>
					</thead>
				<?php 
				if($empleados){
					foreach ($empleados as $empleado) {
				?>		
					<tbody>
						<tr>
						<td>
							<?=$empleado->numeroEmpleado;?>
						</td>
						<td>
							<?=$empleado->nombreEmpleado;?>
						</td>
						<td>
							<?=$empleado->apellidoPaterno;?>
						</td>
						<td>
							<?=$empleado->apellidoMaterno;?>
						</td>
						<td>
							<?=$empleado->roleDescripcion;?>
						</td>
						<td>
							<?=$empleado->tipoDescripcion;?>
						</td>
						<td>
							<a href="modificar/empleados/<?=$empleado->numeroEmpleado?>" tooltip="Agregar Nuevo" class="btn btn-info" href="#" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="fas fa-edit"></i>
						</td>
						<td>
							<a href="eliminar/empleado/<?=$empleado->numeroEmpleado?>" tooltip="Agregar Nuevo" class="btn btn-info" href="#" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="far fa-trash-alt"></i>
						</td>
						<td>
							<a href="movimientos/<?=$empleado->numeroEmpleado?>" class="btn btn-warning" href="#" data-toggle="tooltip" data-placement="top" title="Movimientos"><i class="fas fa-box"></i>
						</td>
						<td>
							<a href="calcular/sueldos/<?=$empleado->numeroEmpleado?>" class="btn btn-warning" href="#" data-toggle="tooltip" data-placement="top" title="Nomina"><i class="fas fa-dollar-sign"></i>
						</td>
						
					</tbody>
				<?php
					}
				}
				?>
				</table>

				</div>
			</div>

	      </div>
	   </div>
	</div>
</body>
</html>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js" ></script>