<?php echo form_open('nuevo/empleado');?>
<div class="container">
  	<div class="row justify-content-center">
    	<div class="col-md-18">
			<div class="card text-center">
				<div class="card-header">
					<h5>Lista de Empleados</h5>
				</div>
				<div class="card-body">
                    <form>
                        <div class="form-row">
                                <label for="nombre">Nombre Empleado</label>
                                    <input type="text" class="form-control"  id="nombre"  name="nombre" placeholder="Nombre de Empleado"  maxlength="50" value="<?php echo set_value('nombre');?>">
                                <div id="nombre">
                                    <?php echo form_error('nombre'); ?>
                                </div> 
                        </div>

                       <div class="form-row">
                             <label for="nombre">Apellido Paterno</label>
                                 <input type="text" class="form-control"  id="appelidoPaterno"  name="appelidoPaterno" placeholder="Apellido Paterno"  maxlength="50" value="<?php echo set_value('appelidoPaterno');?>">
                             <div id="appelidoPaterno">
                                 <?php echo form_error('appelidoPaterno'); ?>
                             </div> 
                       </div>

                       <div class="form-row">
                             <label for="nombre">Apellido Materno</label>
                                 <input type="text" class="form-control"  id="appelidoMaterno"  name="appelidoMaterno" placeholder="Apellido Paterno"  maxlength="50" value="<?php echo set_value('appelidoMaterno');?>">
                             <div id="appelidoMaterno">
                                 <?php echo form_error('appelidoMaterno'); ?>
                             </div> 
                       </div>

                       <div class="form-row">
                             <label for="role">Rol Empleado</label>
                             <select class="form-control" id="role" name="role">
                                <option selected>Seleccionar</option>
                                <option value="1" <?php echo  set_select('role', '1'); ?>>Chofer</option>
                                <option value="2" <?php echo  set_select('role', '2'); ?>>Cargador</option>
                                <option value="3" <?php echo  set_select('role', '3'); ?>>Auxiliar</option>
                            </select>
                            <div id="role">
                                 <?php echo form_error('role'); ?>
                             </div> 
                       </div>

                       <div class="form-row">
                             <label for="tipoEmpleado">Rol Empleado</label>
                             <select class="form-control" id="tipoEmpleado" name="tipoEmpleado">
                                <option selected>Seleccionar</option>
                                <option value="1" <?php echo  set_select('tipoEmpleado', '1'); ?>>Interno</option>
                                <option value="2" <?php echo  set_select('tipoEmpleado', '2'); ?>>Externo</option>
                            </select>
                            <div id="tipoEmpleado">
                                 <?php echo form_error('tipoEmpleado'); ?>
                             </div> 
                       </div>

                        <div class="row">
                            <div class="col">		
                                <div class="card-actions">
                                    <a href="<?php echo base_url();?>empleados" type="submit" tooltip="Agregar Nuevo" class="btn btn-info pull-left"><i class="fas fa-backward"></i> Atras</a>
                                </div>	
                            </div>
                            <div class="col">		
                                <div class="card-actions">
                                    <a  tooltip="Agregar Nuevo" ><button class="btn btn-success pull-right" type="submit" id='registrar' value="registrar"><i class="fas fa-save"></i>  Registrar</button></a>
                                </div>	
                            </div>	
                        </div>	
                    </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js" ></script>