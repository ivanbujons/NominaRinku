<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/fontawesome.css"); ?>" />

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/all.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery-3.3.1.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/config.js" ></script>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="http://localhost/NominaRinku/">Rinku</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>empleados">Empleados</a>
      </li>
    </ul>
  </div>
</nav>
<?php echo $this->session->flashdata('mensaje');?>