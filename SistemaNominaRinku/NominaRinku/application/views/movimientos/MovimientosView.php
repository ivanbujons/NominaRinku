<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Empleados</title>
</head>
	<body>


	<div class="container">
  		<div class="row justify-content-center">
    		<div class="col-md-18">
                <div class="row" >
				
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Empleado: <?=$empleado[0]->nombreEmpleado;?></li>
                        <li class="list-group-item">Rol: <?=$empleado[0]->roleDescripcion;?></li>
                        <li class="list-group-item">Tipo: <?=$empleado[0]->tipoDescripcion;?></li>
                    </ul>
                </div>
				<div class="card text-center">
				<div class="card-header">
					<h5>Lista de Movimientos</h5>
				</div>
				<div class="card-body">

				<div class="row justify-content-left">	
				<div class="card-actions">
                    <a href="<?php echo base_url();?>empleados" type="submit" tooltip="Agregar Nuevo" class="btn btn-info pull-left"><i class="fas fa-backward"></i> Atras</a>
                </div>		
					<div class="card-actions">
						<a href="<?php echo base_url();?>nuevos/movimientos/<?=$empleado[0]->numeroEmpleado?>" tooltip="Agregar Nuevo" class="btn btn-info" href="#">
						<i class="fas fa-box"></i>
						Nuevo
						</a>
					</div>	
				</div>
				
				<table class="table">
					<thead>
						<tr>
						<th scope="col">Número Movimiento</th>
						<th scope="col">Fecha</th>
						<th scope="col">Número Entregas</th>
						<th scope="col">Rol Cubierto</th>
						<th scope="col"></th>
						<th scope="col"></th>
						<th scope="col"></th>
						</tr>
					</thead>
				<?php 
				if($movimientos){
					foreach ($movimientos as $movimiento) {
				?>		
					<tbody>
						<tr>
						<td>
							<?=$movimiento->numeroMovimiento;?>
						</td>
						<td>
							<?=$movimiento->fecha;?>
						</td>
						<td>
							<?=$movimiento->numeroEntregas;?>
						</td>
						<td>
							<?=$movimiento->descripcionRolCubierto;?>
						</td>
						<td>
							<a href="<?php echo base_url();?>eliminar/movimientos/<?=$movimiento->numeroMovimiento.'/'.$empleado[0]->numeroEmpleado?>" tooltip="Agregar Nuevo" class="btn btn-info" href="#" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="far fa-trash-alt"></i>
						</td>
					</tbody>
				<?php
					}
				}
				?>
				</table>

				</div>
			</div>

	      </div>
	   </div>
	</div>
</body>
</html>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js" ></script>