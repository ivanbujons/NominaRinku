<?php echo form_open('nuevos/movimientos/'.$empleado[0]->numeroEmpleado);?>
<div class="container">
  	<div class="row justify-content-center">
    	<div class="col-md-18">
			<div class="card text-center">
				<div class="card-header">
					<h5>Nuevo Movimiento</h5>
				</div>
				<div class="card-body">
                    <form>
                        <div class="form-row">
                                <label for="numeroEmpleado">Número Empleado</label>
                                    <input type="text" readonly class="form-control"  id="numeroEmpleado"  name="numeroEmpleado" value="<?=$empleado[0]->numeroEmpleado;?>">
                        </div>
                        <div class="form-row">
                                <label for="nombre">Nombre Empleado</label>
                                    <input type="text" readonly class="form-control"  id="nombre"  name="nombre" value="<?=$empleado[0]->nombreEmpleado;?>">
                        </div>
                        <div class="form-row">
                                <label for="rol">Rol</label>
                                    <input type="text" readonly class="form-control"  id="rol"  name="rol" value="<?=$empleado[0]->roleDescripcion;?>">
                        </div>
                        <div class="form-row">
                                <label for="tipo">Tipo Empleado</label>
                                    <input type="text" readonly class="form-control"  id="tipo"  name="tipo" value="<?=$empleado[0]->tipoDescripcion;?>">
                        </div>

                       <div class="form-row">
                                    <input type="hidden" readonly class="form-control"  id="idRol"  name="idRol" value="<?=$empleado[0]->idRole;?>">
                        </div>
                        <div class="form-row">
                             <label for="fecha">Fecha Movimiento</label>
                                 <input type="date" class="form-control" min="2018-11-01" max="2050-12-31" id="fecha"  name="fecha" value="<?php echo set_value('fecha');?>">
                             <div id="fecha">
                                 <?php echo form_error('fecha'); ?>
                             </div> 
                       </div>

                       <div class="form-row">
                             <label for="entregas">Número Entregas</label>
                                 <input type="number" class="form-control" min="0" max="1000" id="entregas"  name="entregas" value="<?php echo set_value('entregas');?>">
                             <div id="entregas">
                                 <?php echo form_error('entregas'); ?>
                             </div> 
                       </div>

                       <div class="form-row">
                             <label for="cubrio">Cubre Turno</label>
                       </div>
                       <div class="form-row">
                       <input type="checkbox" class="radio-inline"  onclick="javascript:div_cubre_turno();" name="cubrio" id="cubrio" value="1" <?php echo set_checkbox('cubrio', '1');?>>
                             <div id="cubrio">
                                 <?php echo form_error('cubrio'); ?>
                             </div> 
                       </div>
                       

                       <div class="form-row" id="div_cubre_turno" style="display:none">
                             <label for="role" >Rol Empleado</label>
                             <select class="form-control" id="role" name="role">
                                <option selected>Seleccionar</option>
                                <option value="1" <?php echo  set_select('role', '1'); ?>>Chofer</option>
                                <option value="2" <?php echo  set_select('role', '2'); ?>>Cargador</option>
                            </select>
                            <div id="role">
                                 <?php echo form_error('role'); ?>
                             </div> 
                       </div>

                        <div class="row">
                            <div class="col">		
                                <div class="card-actions">
                                    <a href="<?php echo base_url();?>movimientos/<?=$empleado[0]->numeroEmpleado?>" type="submit" tooltip="Agregar Nuevo" class="btn btn-info pull-left"><i class="fas fa-backward"></i> Atras</a>
                                </div>	
                            </div>
                            <div class="col">		
                                <div class="card-actions">
                                    <a  tooltip="Agregar Nuevo" ><button class="btn btn-success pull-right" type="submit" id='registrar' value="registrar"><i class="fas fa-save"></i>  Registrar</button></a>
                                </div>	
                            </div>	
                        </div>	
                    </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js" ></script>


<script>
function div_cubre_turno() {
    if (document.getElementById('cubrio').checked) {
        document.getElementById('div_cubre_turno').style.display = 'block';
    }
    else {
        document.getElementById('div_cubre_turno').style.display = 'none';
        document.getElementById('div_cubre_turno').value='';

    }
  }
</script>