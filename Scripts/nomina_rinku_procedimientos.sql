
/*Consulta los datos de empleado*/
DELIMITER //
USE nominarinku //
DROP PROCEDURE IF EXISTS proc_consultaempleados //
CREATE PROCEDURE proc_consultaempleados()
BEGIN
	Select numero_empleado, nombre, apellido_paterno, apellido_materno, a.id_role, b.descripcion as role_descripcion, a.id_tipo, c.descripcion as tipo_descripcion
	FROM empleados a 
	JOIN roles b
	on a.id_role = b.id_role
	JOIN tipo_empleados c
	on a.id_tipo = c.id_tipo;
END //
DELIMITER ;


/*Busca Empleado*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_obtenerempleado //
CREATE PROCEDURE proc_obtenerempleado(
IN numeroEmpleado INT)
BEGIN
	Select numero_empleado, nombre, apellido_paterno, apellido_materno, a.id_role, b.descripcion as role_descripcion, a.id_tipo, c.descripcion as tipo_descripcion
	FROM empleados a 
	JOIN roles b
	on a.id_role = b.id_role
	JOIN tipo_empleados c
	on a.id_tipo = c.id_tipo
	WHERE numero_empleado = numeroEmpleado;
END //
DELIMITER ;


/*Guardar Empleado*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_guardarempleado //
CREATE PROCEDURE proc_guardarempleado(
	  IN param_nombre VARCHAR(50)
	, IN param_apellido_paterno VARCHAR(50)
	, IN param_apellido_materno VARCHAR(50)
	, IN param_id_role INT
	, IN param_id_tipo INT
	)
BEGIN
	INSERT INTO empleados (
	  nombre
	, apellido_paterno
	, apellido_materno
	, id_role
	, id_tipo) 
	VALUES(
	  param_nombre
	, param_apellido_paterno
	, param_apellido_materno
	, param_id_role
	, param_id_tipo);
END //
DELIMITER ;

/*Eliminar Empleado*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_eliminarempleado //
CREATE PROCEDURE proc_eliminarempleado(
IN param_numero_empleado INT)
BEGIN
	DELETE FROM empleados WHERE numero_empleado = param_numero_empleado;
END //
DELIMITER ;

/*Modificar Empleado*/
DELIMITER //
USE nominarinku //
DROP PROCEDURE IF EXISTS proc_modificarempleado //
CREATE PROCEDURE proc_modificarempleado(
	  IN param_numero_empleado INT
	, IN param_nombre VARCHAR(50)
	, IN param_apellido_paterno VARCHAR(50)
	, IN param_apellido_materno VARCHAR(50)
	, IN param_id_role INT
	, IN param_id_tipo INT
	)
BEGIN
	UPDATE empleados SET
	  nombre = param_nombre
	, apellido_paterno = param_apellido_paterno
	, apellido_materno = param_apellido_materno
	, id_role = param_id_role
	, id_tipo = param_id_tipo
	WHERE numero_empleado = param_numero_empleado;
END //
DELIMITER ;


/*Consulta los datos de movimientos*/
DELIMITER //
USE nominarinku //
DROP PROCEDURE IF EXISTS proc_consultamovimientos //
CREATE PROCEDURE proc_consultamovimientos(
	IN param_numero_empleado INT)
BEGIN
	SELECT a.numero_empleado, nombre, apellido_paterno, apellido_materno, a.id_role, b.descripcion as role_descripcion, a.id_tipo
	     , c.descripcion as tipo_descripcion, d.id_movimiento, d.fecha, d.entregas, d.rol_cubierto, f.descripcion as descripcion_rol_cubierto
	FROM empleados a 
	JOIN roles b
	ON a.id_role = b.id_role
	JOIN tipo_empleados c
	ON a.id_tipo = c.id_tipo
	JOIN movimientos d
	ON a.numero_empleado = d.numero_empleado
	JOIN roles f
	ON d.rol_cubierto = f.id_role
	WHERE a.numero_empleado = param_numero_empleado;
END //
DELIMITER ;


/*Guardar movimientos*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_guardarmovimiento //
CREATE PROCEDURE proc_guardarmovimiento(
	  IN param_numero_empleado VARCHAR(50)
	, IN param_fecha VARCHAR(50)
	, IN param_entrega VARCHAR(50)
	, IN param_rol_cubierto INT
	)
BEGIN
	INSERT INTO movimientos (
	  numero_empleado
	, fecha
	, entregas
	, rol_cubierto) 
	VALUES(
	  param_numero_empleado
	, param_fecha
	, param_entrega
	, param_rol_cubierto);
END //
DELIMITER ;


/*Eliminar Empleado*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_eliminarmovimiento //
CREATE PROCEDURE proc_eliminarmovimiento(
IN param_numero_movimiento INT)
BEGIN
	DELETE FROM movimientos WHERE id_movimiento = param_numero_movimiento;
END //
DELIMITER ;


/*obtener Entregas*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_obtenerentregas //
CREATE PROCEDURE proc_obtenerentregas(
	  IN param_numero_empleado INT
	, IN param_anio INT
	, IN param_mes INT)
BEGIN
	SELECT sum(entregas) as entregas
	FROM movimientos WHERE YEAR(fecha) = param_anio AND MONTH(fecha) = param_mes
	AND numero_empleado = param_numero_empleado;
END //
DELIMITER ;

/*obtener dias que cubrio*/
DELIMITER //
use nominarinku //
DROP PROCEDURE IF EXISTS proc_obtenerdias //
CREATE PROCEDURE proc_obtenerdias(
	  IN param_numero_empleado INT
	, IN param_anio INT
	, IN param_mes INT)
BEGIN
	SELECT COUNT(fecha) as dias, rol_cubierto
	FROM movimientos WHERE YEAR(fecha) = param_anio AND MONTH(fecha) = param_mes
	AND numero_empleado = param_numero_empleado
	AND rol_cubierto != 3
	group by rol_cubierto;
END //
DELIMITER ;
























