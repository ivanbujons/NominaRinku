/*CREATE DATABASE nominaRinku;*/
USE nominaRinku;


/*
Creación de tablas Sistema Nómina Rinku
*/

CREATE TABLE roles (
	id_role INT(5) NOT NULL,
	descripcion VARCHAR(50) NOT NULL,
	PRIMARY KEY (id_role)
);

CREATE TABLE tipo_empleados(
	id_tipo INT(5) NOT NULL,
	descripcion VARCHAR(50) NOT NULL,
	PRIMARY KEY (id_tipo)
);

CREATE TABLE empleados (
   numero_empleado INT NOT NULL AUTO_INCREMENT,
   nombre VARCHAR(50) NOT NULL,
   apellido_paterno VARCHAR(50) NOT NULL,
   apellido_materno VARCHAR(50) NOT NULL,
   id_role INT(5) NOT NULL,
   id_tipo INT(5) NOT NULL,
   PRIMARY KEY (numero_empleado),
   CONSTRAINT id_rol_empleado FOREIGN KEY(id_role) REFERENCES roles(id_role),
   CONSTRAINT id_tipo_empleado FOREIGN KEY(id_tipo) REFERENCES tipo_empleados(id_tipo)
);

CREATE TABLE movimientos(
  id_movimiento INT NOT NULL AUTO_INCREMENT,
  numero_empleado INT NOT NULL,
  fecha DATE NOT NULL,
  entregas INT NOT NULL,
  rol_cubierto INT(5) NOT NULL,
  PRIMARY KEY (id_movimiento)
);


/*Se precargan datos*/

INSERT INTO roles (id_role, descripcion) VALUES
(1, 'Chofer'),
(2, 'Cargador'),
(3, 'Auxiliar');

INSERT INTO tipo_empleados (id_tipo, descripcion) VALUES
(1, 'Interno'),
(2, 'Externo');

INSERT INTO empleados (numero_empleado, nombre, apellido_paterno, apellido_materno, id_role, id_tipo) VALUES
(1,'Ivan','Leyva','Bujons',1,1),
(2,'Alberto','Bujons','Leyva',2,2);

INSERT INTO movimientos(numero_empleado, fecha, entregas, rol_cubierto)VALUES
(1,'2018-10-20',10,2),
(2,'2018-10-25',30,1);